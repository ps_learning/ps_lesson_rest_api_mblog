# from flask import Flask, jsonify

# twits = []

# app = Flask(__name__)

# @app.route('/twit', methods=['POST'])
# def create_twit():
#     ''{"body": "Hello World", "author": "@aqaguy"}
#     ''
#     twit_json = request.get_json()
#     twit = Twit(twit_json['body'], twit_json['author'])
#     twits.append(twit)
#     return jsonify({'status': 'success'})

# @app.route('/twit', methods=['GET'])
# def read_twit():
#     return jsonify({'twits': twits})

# if __name__ == '__main__':
#     app.run(debug=True)

from flask import Flask, request, jsonify

# Класс для представления твитов
class Twit:
    def __init__(self, body, author):
        self.body = body
        self.author = author

    def to_dict(self):
        return {'body': self.body, 'author': self.author}

# Список для хранения твитов
twits = []

app = Flask(__name__)

@app.route('/twit', methods=['POST'])
def create_twit():
    """
    Создание нового твита.
    Ожидаемый формат запроса:

    {"body": "Hello World", "author": "@aqaguy"}
    {"body": "Hello my first REST API", "author": "@aqaguy"}

    """
    twit_json = request.get_json()
    twit = Twit(twit_json['body'], twit_json['author'])
    twits.append(twit)
    return jsonify({'status': 'success'})

@app.route('/twit', methods=['GET'])
def read_twit():
    """
    Получение всех твитов.
    """
    return jsonify({'twits': [twit.to_dict() for twit in twits]})

if __name__ == '__main__':
    app.run(debug=True)
